<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagController extends Controller
{
    /**
     * @Route("/{tag}", name="tag")
     */
    public function indexAction($tag)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $paginator  = $this->get('knp_paginator');
        $tagRepository = $this->getDoctrine()->getRepository(Tag::class);
        $tags = $tagRepository->findTopTags(10);
        $obTag = $tagRepository->findOneBy(['name'=> $tag],[]);

        if( !$obTag instanceof Tag ){
            throw $this->createNotFoundException('this tag doesn\'t exist.');
        }

        $query = $this->getDoctrine()->getRepository(Post::class)->getQueryFindPostsByTag($obTag->getId());

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('tag/index.html.twig', [
            'tag' => $obTag->getName(),
            'pagination' => $pagination,
            'tags' => $tags
        ]);
    }
}
