<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomePageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $tags = $this->getDoctrine()->getRepository(Tag::class)->findTopTags(10);
        $paginator  = $this->get('knp_paginator');
        $search = $request->get('search');

        $query = $this->getDoctrine()->getRepository(Post::class)->getQueryPostsList($search);

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('homePage/index.html.twig', [
            'tags' => $tags,
            'pagination' => $pagination,
            'search' => $search,

        ]);
    }
}
