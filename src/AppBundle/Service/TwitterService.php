<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use TwitterAPIExchange;
use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;

class TwitterService
{
    protected $em;
    protected $doctrine;

    public function __construct(RegistryInterface $doctrine,  EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->doctrine = $doctrine;
    }

   public function getPostsFromTwitter($twitterParams, $count = 200){

       $postRepository = $this->doctrine->getRepository(Post::class)->FindOneBy([],['createdAt' => 'desc']);
       if(!empty($postRepository)) {
           $olderId = $postRepository->getExternalId();
       }else{
           $olderId = '';
       }
       $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
       $getfield = "?screen_name=bbcrussian&count=$count&since_id=$olderId";
       $requestMethod = 'GET';

       $twitter = new TwitterAPIExchange($twitterParams);
       $response = $twitter->setGetfield($getfield)
           ->buildOauth($url, $requestMethod)
           ->performRequest();

       self::savePosts($response);

       return count(json_decode($response));
   }

   private function savePosts($response){
        $tagRepository = $this->doctrine->getRepository(Tag::class);
        $postRepository = $this->doctrine->getRepository(Post::class);

        $posts = json_decode($response);

        foreach ($posts as $post){

            $savedPost = $postRepository->FindOneBy(['externalId' => $post->id_str]);

            if(!$savedPost instanceof Post){
                $newPost = new Post;
                $newPost->setDescription($post->text);
                $newPost->setCreatedAt(new \DateTime($post->created_at));
                $newPost->setExternalId($post->id);

                if(!empty($post->entities->hashtags)){
                    $tags = $post->entities->hashtags;
                    foreach ($tags as $tag){
                        $savedTag = $tagRepository->FindOneBy(['name' => $tag->text]);

                        if(!$savedTag instanceof Tag){
                            $newTag = new Tag;
                            $newTag->setName($tag->text);
                            $savedTag = $newTag;
                            $this->em->persist($savedTag);
                            $this->em->flush();
                        }

                        $newPost->addTag($savedTag);
                    }
                }
                $this->em->persist($newPost);
            }
        }
       $this->em->flush();
   }
}