<?php

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetNewsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:get_news')
            ->setDescription('Get news from twitter');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $twitterParams =  $this->getContainer()->getParameter('twitter');
        $count = $this->getContainer()->get('AppBundle\Service\TwitterService')->getPostsFromTwitter($twitterParams);
        $output->writeln('<info>' . $count . ' post(s) imported</info>');

    }
}
